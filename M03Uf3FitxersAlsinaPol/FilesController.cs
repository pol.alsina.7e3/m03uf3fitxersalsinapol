﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03Uf3FitxersAlsinaPol
{
    /// <summary>
    /// Clase controla els fitxers
    /// </summary>
    class FilesController
    {
        /// <summary>
        /// crear fitxer i l'omple
        /// </summary>
        /// <param name="contentFile">contingut a omplir</param>
        /// <param name="nameFile">nom del fitxer</param>
        public void CreateFileOddOrEven(List<int> contentFile, string nameFile)
        {
            StreamWriter streamWriter = new StreamWriter(nameFile);
            foreach (var s in contentFile)
            {
                streamWriter.WriteLine(s);
            }
            streamWriter.Close();
        }
        /// <summary>
        /// Afegeix una linea al fitxer inventari
        /// </summary>
        /// <param name="line">linea afeigir</param>
        public void AppendLineToInventory(string line)
        {
            string path = @"E:\My Way\Primer\m03uf3fitxersalsinapol\M03Uf3FitxersAlsinaPol\Files\inventari.txt"; ;
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(line);
            }
            
        }
        /// <summary>
        /// llegeix el fitxer de configuracio
        /// </summary>
        /// <param name="path">path del fitxer</param>
        /// <param name="name">nom a printar</param>
        public void ReadConfigFile(string path, ref string name)
        {
            string text = File.ReadAllText(path);
            string[] textSplit = text.Split('\n',':');
            name = textSplit[1];
        }
        /// <summary>
        /// llegeix al file i printa
        /// </summary>
        /// <param name="path">path del fitxer</param>
        /// <param name="name">nom a printar</param>
        public void ReadFileAndPrintName(string path, string name)
        {
            string text = File.ReadAllText(path);
            Console.WriteLine(text + "" + name);
        }
        /// <summary>
        /// llegeix fitxer
        /// </summary>
        /// <param name="path">path del fitxer</param>
        /// <returns>un llista amb les dades del fitxer</returns>
        public List<String> ReadFileTextToList(string path)
        {
            
            string[] readFile = File.ReadAllLines(path);
            string[] split = readFile[0].Split(' ', '.',',');
            List<string> secretWords = new List<string>();
            foreach (var s in split)
            {
                secretWords.Add(s);
            }
            return secretWords;
        }
        /// <summary>
        /// buscar un fitxer recursivament
        /// </summary>
        /// <param name="target">path a on buscar</param>
        /// <param name="fileToSearch">fitxer a buscar</param>
        public void FindFile(string target, string fileToSearch)
        {
            DirectoryInfo carpeta = new DirectoryInfo(target);
            DirectoryInfo[] dires = carpeta.GetDirectories();
            IEnumerable<FileInfo> files = carpeta.EnumerateFiles("*.*", SearchOption.TopDirectoryOnly);
            foreach (FileInfo file in files)
            {
                if (file.Name == fileToSearch)
                {
                    Console.WriteLine(file.DirectoryName + " " + file.Name);
                }
            }    

            if (Directory.Exists(target))
            {
                foreach (var subdir in dires)
                {
                    string combine = Path.Combine(target, subdir.Name);
                    FindFile(combine, fileToSearch);
                }
            }
        }

    }
}
