﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03Uf3FitxersAlsinaPol
{
    /// <summary>
    /// clase que s'ocupa a printar i ha agafar dades
    /// </summary>
    class InData
    {
        /// <summary>
        /// mostra les opcions del menu
        /// </summary>
        public void MostrarMenu()
        {
            Console.WriteLine("-----Fitxers-----");
            Console.WriteLine("1-Parell i senar");
            Console.WriteLine("2-Inventario");
            Console.WriteLine("3-WORDCOUNT");
            Console.WriteLine("4-ConfigFile Language");
            Console.WriteLine("5-FindPaths");
            Console.WriteLine("0-Per sortir del programa");
        }
        /// <summary>
        /// Metode que llegeix un numero per a al menu
        /// </summary>
        /// <returns>Retorna un numero que equival a l'opcio del menu</returns>
        public int OpcioMenuInt()
        {
            int opcio;
            do
            {
                opcio = Convert.ToInt32(Console.ReadLine());
            } while (opcio < 0 || opcio > 5);
            return opcio;
        }
        /// <summary>
        /// Mira si el numero es parell
        /// </summary>
        /// <param name="checkNum">numero a mirar si es parell</param>
        /// <returns>true es parell false senar</returns>
        public bool Isparell(int checkNum)
        {
            if ((checkNum % 2) == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// converteix un string a int
        /// </summary>
        /// <param name="toConvert">num a convertir</param>
        /// <returns>el numero convertit</returns>
        public int ConvertStringToInt(string toConvert)
        {
            int result = 0;
            try
            {
                result = Int32.Parse(toConvert);
                Console.WriteLine(result);
                return result;
            }
            catch (FormatException)
            {
                Console.WriteLine("End file");
            }
            return result;
        }
        /// <summary>
        /// llegeix string
        /// </summary>
        /// <param name="message">missatge a printar</param>
        /// <returns>el string llegit</returns>
        public string LlegirString(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }
        /// <summary>
        /// llegeix int
        /// </summary>
        /// <param name="message">missatge a printar</param>
        /// <returns>el string llegit</returns>
        public int LlegirInt(string message)
        {
            Console.WriteLine(message);
            return Convert.ToInt32(Console.ReadLine());
        }
        /// <summary>
        /// llegeix string per omplir en el fitxer inventari
        /// </summary>
        /// <returns>la linea per omplir</returns>
        public string FillInventoryLine()
        {
            string line = LlegirString("Introdueix el nom")+", ";
            line += LlegirString("Introdueix el model") + ", ";
            line += LlegirString("Introdueix la MAC") + ", ";
            line += LlegirInt("Any de fabricacio").ToString();
            return line;
        }
    }
}

