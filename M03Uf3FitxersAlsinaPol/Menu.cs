﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03Uf3FitxersAlsinaPol
{
    class Menu
    {
        InData _dades = new InData();

        // <summary>
        /// Metode que incialitzar el menu
        /// </summary>

        public void WhileMenu()
        {
            bool finish;
            do
            {
                _dades.MostrarMenu();
                finish = TractarOpcio();
            } while (!finish);
        }
        /// <summary>
        /// Metode que tractar les diferents opcions que te el menu i accedeix a  altres metodes
        /// </summary>
        /// <returns>Retorna un boolea. Si es true acabar el programa, si no continua</returns>
        public bool TractarOpcio()
        {
            Exercicis exercicis = new Exercicis();
            int opcio = _dades.OpcioMenuInt();
            switch (opcio)
            {
                case 1:
                    exercicis.Exercici1();
                    break;
                case 2:
                    exercicis.Exercici2();
                    break;
                case 3:
                    exercicis.Exercici3();
                    break;
                case 4:
                    exercicis.Exercici4();
                    break;
                case 5:
                    exercicis.Exercici5();
                    break;
                case 0:
                    return true;
            }
            return false;
        }

    }
}
