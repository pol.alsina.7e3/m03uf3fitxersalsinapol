﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M03Uf3FitxersAlsinaPol
{
    /// <summary>
    /// Clase per la execucio dels exercicis
    /// </summary>
    class Exercicis
    {
        InData data = new InData();
        FilesController files = new FilesController();
        /// <summary>
        /// Metodes per l'execucio del exercici1
        /// </summary>
        public void Exercici1()
        {
            List<int> parell = new List<int>();
            List<int> senar = new List<int>();
            string pathNumbers = @"E:\My Way\Primer\m03uf3fitxersalsinapol\M03Uf3FitxersAlsinaPol\Files\Numbers.txt";
            foreach (string line in File.ReadLines(pathNumbers))
            {
                int num = data.ConvertStringToInt(line);
                if (data.Isparell(num))
                {
                    parell.Add(num);
                }
                else
                {
                    senar.Add(num);
                }
            }
            files.CreateFileOddOrEven(parell, @"E:\My Way\Primer\m03uf3fitxersalsinapol\M03Uf3FitxersAlsinaPol\Files\parell.txt");
            files.CreateFileOddOrEven(senar, @"E:\My Way\Primer\m03uf3fitxersalsinapol\M03Uf3FitxersAlsinaPol\Files\senar.txt");
          
        }
        /// <summary>
        /// Metodes per l'execucio del exercici2
        /// </summary>
        public void Exercici2()
        {
            string lineToText = data.FillInventoryLine();
            files.AppendLineToInventory(lineToText);

        }
        /// <summary>
        /// Metodes per l'execucio del exercici3
        /// </summary>
        public void Exercici3()
        {
            string path = @"E:\My Way\Primer\m03uf3fitxersalsinapol\M03Uf3FitxersAlsinaPol\Files\wordcount.txt";
            var text = files.ReadFileTextToList(path);
            string llegirParaula;
            do
            {
                llegirParaula = data.LlegirString("Introdueix una paraula per buscar al text");
                string[] split_text = llegirParaula.Split(' ');

                for (int i = 0; i < split_text.Length; i++)
                {
                    int countWord = 0;
                    foreach (string word in text)
                    {
                        if (word.ToLower() == split_text[i])
                        {
                            countWord++;
                        }
                    }
                    Console.WriteLine(split_text[i] + ": " + countWord);
                }
            } while (llegirParaula != "END");
        }
        /// <summary>
        /// Metodes per l'execucio del exercici4
        /// </summary>
        public void Exercici4()
        {
            string name = " ";  
            string path = @"E:\My Way\Primer\m03uf3fitxersalsinapol\M03Uf3FitxersAlsinaPol\Files\config.txt";
            string pathEsp = @"E:\My Way\Primer\m03uf3fitxersalsinapol\M03Uf3FitxersAlsinaPol\Files\lang\lng_es.txt";
            string pathCat = @"E:\My Way\Primer\m03uf3fitxersalsinapol\M03Uf3FitxersAlsinaPol\Files\lang\lng_cat.txt";
            files.ReadConfigFile(path, ref name);
            string idioma = data.LlegirString("Idioma [cat / es] ?").ToLower();
            if (idioma == "cat")
            {
                files.ReadFileAndPrintName(pathCat, name);
            }
            else if (idioma == "es")
            {
                files.ReadFileAndPrintName(pathEsp, name);
            }
            else
            {
                files.ReadFileAndPrintName(pathCat, name);
            }

        }
        /// <summary>
        /// Metodes per l'execucio del exercici5
        /// </summary>
        public void Exercici5()
        {
            string nomFitxer = data.LlegirString("Quin és el nom del fitxer a cercar?");
            string path = data.LlegirString("Escriu el nom d'una ruta a una carpeta:");
            files.FindFile(path,nomFitxer);
        }
    }
}
